/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kuroinnu.remindbyemail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class HomeController {
    @Autowired
    private JavaMailSender javaMailSender;
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        //sendEmail();
        return "index";
    }
       
    void sendEmail() {

        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo("andrei.gavrilov.kk@gmail.com");

        msg.setSubject("Testing from Spring Boot");
        msg.setText("Hello World \n Spring Boot Email");

        
        javaMailSender.send(msg);

    }
    
}
